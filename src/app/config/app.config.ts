/** Global configuration of application */
import {environment} from '../../environments/environment';

export const AppConfig = {
  /** End points */
  endPoints: {
    userList: `${environment.apiUrl}/users`,
    singleUser: (id) => `${environment.apiUrl}/users/${id}`,
  }
};

