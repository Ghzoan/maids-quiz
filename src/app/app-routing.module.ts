import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './modules/users/pages/list/user-list.component';
import {UserListResolver} from "./modules/users/resolvers/user-list.resolver";

const routes: Routes = [

  {
    path: 'users',
    loadChildren: './modules/users/user.module#UserModule'
  },
  {
    path: '',
    component: UserListComponent,
    resolve: {
      data: UserListResolver
    }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
