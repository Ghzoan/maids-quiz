import {Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ListingConfigActionInterface, ListingConfigInterface} from '../../../../models/list-config.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*', padding: '0px', margin: '0px' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ListComponent implements OnInit, OnChanges {
  /** All list configuration of list. */
  @Input() config: ListingConfigInterface;
  /** Data to show up in the list. */
  @Input() data: [any];
  /** When click on action button. */
  @Output() onAction = new EventEmitter();
  /** Item of details to show. */
  details = [];
  /** Row headers. */
  listing = [];
  expanded_element = null;

  /**
   * Create a new instance from UserListComponent
   * @constructor
   * @public
   */
  constructor() { }

  /**
   * When component initialized.
   * @return {void}
   */
  ngOnInit(): void {
  }

  /**
   * When component change
   * @return {void}
   */
  ngOnChanges(changes: SimpleChanges): void {
    console.log('asd');
    if (changes.config) {
      this.cleanConfig();
    } else if (changes.data) {
      this.formatListingData();
    }
  }

  /**
   * Clear configuration of the list
   * @return {void}
   */
  cleanConfig(): void {
    let uid = 0;
    // generate ids for each listing item.
    this.config.listing.map(item => {
      const tid = this.generateId(item, ++uid);
      this.listing.push(tid);
      return item.id = tid;
    });
    this.formatListingData();
  }

  /**
   * Generate an id for each id
   * @param {any} item
   * @param {number} uid
   * @return {any}
   */
  generateId(item: any, uid: number): any {
    return typeof item.value === 'function' ? `i-${Math.floor(Math.random() * 10000)}-${uid}` : item.value;
  }

  /**
   * Formatting data by the list configuration.
   * @return {void}
   */
  formatListingData(): void {
    if (!this.config.listing || !this.config.listing.length || !this.data || !this.data.length) {
      return;
    }
    this.data.map((row, index) => {
      // set index
      row.index = index;
      // set the action visibility state on each data item.
      row = this.handleActionVisibility(row);
      // set row bg
      row = this.handleRowBackground(row);
      // set the clean data for details listing
      row = this.handleRowDetails(row);
      // return the formatted row.
      return this.format(row);
    });
  }

  /**
   * Handle visibility of each action in action list.
   * @param {any} row
   * @return {any}
   */
  handleActionVisibility(row: any): any {
    row._actions = {};
    this.config.actions.forEach(action => {
      if (typeof action.visible === 'function') {
        row._actions[action.action] = action.visible(row);
      } else if (typeof action.visible === 'undefined') {
        row._actions[action.action] = true;
      } else {
        row._actions[action.action] = action.visible;
      }
    });
    return row;
  }

  /**
   * Handle the background color of row
   * @param {any} row
   * @return {any}
   */
  handleRowBackground(row: any): any {
    if (typeof this.config.background === 'function') {
      row._bg = this.config.background(row);
      if (!row._bg) {
        row._bg = row.index % 2 ? '#ffff' : '#f8f8f8';
      }
    } else if (typeof this.config.background !== 'undefined') {
      row._bg = this.config.background;
    } else {
      row._bg = row.index % 2 ? '#ffff' : '#f8f8f8';
    }
    return row;
  }

  /**
   * Get details of row by list configuration.
   * @param {any} row
   * @return {any}
   */
  handleRowDetails(row: any): any {
    row._details = [];
    this.config.details.forEach(item => {
      // handle detail visibility
      if (item.visible === false || (typeof item.visible === 'function' && !item.visible(row))) {
        return;
      }
      const val = typeof item.value === 'function' ? item.value(row) : row[item.value];
      row._details.push({
        label: item.label,
        value: (val === 'null') || !val ? '-' : val
      });
    });
    return row;
  }

  /**
   * Formatting value of the row by list configuration.
   * @param {any} row
   * @return {any}
   */
  format(row: any): any {
    this.config.listing.forEach(item => {
      if (typeof item.value === 'function') {
        row[item.id] = item.value(row);
      }
      return;
    });
    return row;
  }

  /**
   * When click to expand element
   * @param {any} elm
   * @return {void}
   */
  expand(elm: any): void{
    this.expanded_element = this.expanded_element === elm ? null : elm;
  }

  /**
   * When click to apply action
   * @param {ListingConfigActionInterface} action
   * @param {any} row
   * @return {void}
   */
   actionClicked(action: ListingConfigActionInterface, row: any): void {
    if (typeof action.handler === 'function') {
        action.handler(row);
    }
  }

  cellActionFired(cellAction, element, $event) {
    $event.stopPropagation();
    if (typeof cellAction.click === 'function') {
      cellAction.click(element);
    }
  }
}
