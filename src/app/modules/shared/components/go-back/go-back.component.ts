import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-go-back',
  templateUrl: './go-back.component.html',
  styleUrls: ['./go-back.component.css']
})
export class GoBackComponent implements OnInit {

  /**
   * Create a new instance from GoBackComponent
   * @constructor
   * @public
   * @param {Location} location
   */
  constructor(private location: Location) {}

  ngOnInit(): void {
  }

  /**
   * Go to previous location
   * @return {void}
   */
  backClicked(): void {
    this.location.back();
  }
}
