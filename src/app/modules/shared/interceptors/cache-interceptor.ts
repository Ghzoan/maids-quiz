import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';

export class CacheInterceptor implements HttpInterceptor {
  /** Cash map */
  private cache: Map<string, HttpResponse<any>> = new Map();

  /**
   * Apply cache logic before sending request
   * @override
   * @public
   * @param {HttpRequest<any>} req
   * @param {HttpHandler} next
   * @return {Observable<HttpEvent<any>>}
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Check on method of request.
    if (req.method !== 'GET') {
      return next.handle(req);
    }
    // You can skip cache with set header skip_cache.
    if (req.headers.get('skip_cache')) {
      // Delete cached response from map.
      this.cache.delete(req.urlWithParams);
    }
    // Check on cached response
    const cachedResponse: HttpResponse<any> = this.cache.get(req.urlWithParams);
    if (cachedResponse) {
      // Return cached response
      return of(cachedResponse.clone());
    }else {
      // Apply request and store response in map.
      return next.handle(req).pipe(
        tap((state) => {
            if (state instanceof HttpResponse){
              this.cache.set(req.urlWithParams, state.clone());
            }
        })
      );
    }
  }
}
