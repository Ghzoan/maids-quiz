import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import {MatTableModule} from '@angular/material/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCardModule} from '@angular/material/card';
import { GoBackComponent } from './components/go-back/go-back.component';
import {MatButtonModule} from "@angular/material/button";



@NgModule({
  declarations: [ListComponent, GoBackComponent],
    imports: [
        CommonModule,
        MatTableModule,
        MatTooltipModule,
        MatIconModule,
        MatButtonToggleModule,
        MatPaginatorModule,
        MatCardModule,
        MatButtonModule,

    ], exports: [
    ListComponent,
    GoBackComponent
  ]
})
export class SharedModule { }
