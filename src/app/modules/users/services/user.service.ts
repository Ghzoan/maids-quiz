import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {AppConfig} from '../../../config/app.config';
import {Observable} from 'rxjs';
import {ResponseShapeModel} from '../../../models/response-shape.model';
import {ListingConfigInterface} from "../../../models/list-config.model";
import {Router} from "@angular/router";
import {User} from "../../../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  /**
   * Create a new instance from user service.
   * @constructor
   * @public
   * @param {HttpClient} http
   * @param {Router} router
   */
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  /**
   * Get user list from server-side
   * @param {number} page
   * @return {Observable<HttpResponse<ResponseShapeModel>>}
   */
  getUserList(page: number): Observable<HttpResponse<ResponseShapeModel>> {
    return this.http.get(AppConfig.endPoints.userList, {
      params: {
        page: String(page)
      },
      observe: 'response'
    });
  }

  /**
   * Get single user details from server-side
   * @param {number} id  Id of user.
   * @return {Observable<ResponseShapeModel>}
   */
  getSingleUserDetails(id: number): Observable<ResponseShapeModel>{
    return this.http.get(AppConfig.endPoints.singleUser(id));
  }

  /**
   * Get a configuration for user lists.
   * @return {ListingConfigInterface}
   */
  getUserListConfig(): ListingConfigInterface{
    return {
      listing: [
        {
          label: 'ID',
          value: (row: User) => row.id ? row.id : '-',
        },
        {
          label: 'Full name',
          value: (row: User) => row ? `${row.first_name} ${row.last_name}` : '-',
        },
        {
          label: 'Email',
          value: (row: User) => row ? row.email : '-',
        }
      ],
      actions: [
        {
          action: 'view',
          label: 'View user details',
          icon: 'search',
          color: 'primary',
          handler: (row: User) => this.router.navigate(['/users/' + row.id + '/profile']),
          visible: true
        }
      ],
      details: [
        {
          label: 'ID',
          value: (row: User) => row.id ? row.id : '-',
          visible: true,
        },
        {
          label: 'Full name',
          value: (row: User) => row ? `${row.first_name} ${row.last_name}` : '-',
          visible: true
        },
        {
          label: 'Email',
          value: (row: User) => row ? row.email : '-',
          visible: true
        }
      ]
    };
  }

  /**
   * Classify results after fetch it from serve-sid
   * @param {[]} results
   * @return {User[]}
   */
  classifyResults(results: []): User[]{
    const users = [];
    results.forEach((item) => {
      users.push(new User(item));
    });
    return users;
  }
}
