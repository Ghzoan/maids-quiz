import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './pages/list/user-list.component';
import {UserListResolver} from "./resolvers/user-list.resolver";
import {UserProfileComponent} from "./pages/user-profile/user-profile.component";
import {UserProfileResolver} from "./resolvers/user-profile.resolver";

const routes: Routes = [
  {
    path: 'list',
    component: UserListComponent,
    resolve: {
      data: UserListResolver
    }
  },
  {
    path: ':id/profile',
    component: UserProfileComponent,
    resolve: {
      data: UserProfileResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
