import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './pages/list/user-list.component';
import {SharedModule} from '../shared/shared.module';
import {MatCardModule} from "@angular/material/card";
import {MatPaginatorModule} from "@angular/material/paginator";
import {HttpClientModule} from "@angular/common/http";
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {UserListResolver} from "./resolvers/user-list.resolver";
import {MatSelectModule} from "@angular/material/select";
import {FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import { NgMatSearchBarModule } from 'ng-mat-search-bar';
import {MatFormFieldControl} from "@angular/material/form-field";
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import {UserProfileResolver} from './resolvers/user-profile.resolver';
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [UserListComponent, UserProfileComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    MatCardModule,
    MatPaginatorModule,
    HttpClientModule,
    MatProgressBarModule,
    MatSelectModule,
    FormsModule,
    MatButtonModule,
    MatTooltipModule,
    NgMatSearchBarModule,
    ReactiveFormsModule,
    MatIconModule,
  ], providers: [
    UserListResolver,
    UserProfileResolver
  ]
})
export class UserModule { }
