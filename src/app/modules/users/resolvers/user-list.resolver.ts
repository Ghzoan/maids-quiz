import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable, OnDestroy} from '@angular/core';
import {Observable, Subject} from "rxjs";
import {User} from '../../../models/user.model';
import {UserService} from "../services/user.service";
import {takeUntil} from "rxjs/operators";

@Injectable()
export class UserListResolver implements Resolve<any>, OnDestroy{
  /** Subject instance. */
  unsubscribeAll: Subject<any> = new Subject();

  /**
   * Create a new instance from user list resolver.
   * @constructor
   * @public
   * @param {UserService} userService
   */
  constructor(
   private userService: UserService
  ) {
  }

  /**
   * Resolve user list when call route
   * @public
   * @override
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @return {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      this.userService.getUserList(1).pipe(takeUntil(this.unsubscribeAll))
        .subscribe(
          results => {
              resolve(results.body);
          }, error => {
              reject(error);
          }
        );
    });
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }
}
