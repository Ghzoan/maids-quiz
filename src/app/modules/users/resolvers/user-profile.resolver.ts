import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable, OnDestroy} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {UserService} from "../services/user.service";
import {takeUntil} from "rxjs/operators";

@Injectable()
export class UserProfileResolver implements Resolve<any>, OnDestroy{

  /** Subject instance. */
  unsubscribeAll: Subject<any> = new Subject();

  /**
   * Create a new instance from user list resolver.
   * @constructor
   * @public
   * @param {UserService} userService
   */
  constructor(
    private userService: UserService
  ) {
  }

  /**
   * Resolve user profile when call route
   * @public
   * @override
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @return {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      const userId = route.paramMap.get('id');
      this.userService.getSingleUserDetails(parseInt(userId, 10)).pipe(takeUntil(this.unsubscribeAll))
        .subscribe(
          results => {
            resolve(results.data);
          }, error => {
            reject(error);
          }
        );
    });
  }


  ngOnDestroy(): void {
  }

}
