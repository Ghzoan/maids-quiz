import { Component, OnInit } from '@angular/core';
import {User} from "../../../../models/user.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  /** Current User */
  user: User;

  /**
   * Create a new instance from list component
   * @constructor
   * @public
   * @param {ActivatedRoute} route
   */
  constructor(
    private route: ActivatedRoute
  ) {
    this.user = new User(this.route.snapshot.data.data);
  }

  ngOnInit(): void {
  }

}
