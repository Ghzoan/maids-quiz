import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from "rxjs";
import {ListingConfigInterface} from '../../../../models/list-config.model';
import {User} from '../../../../models/user.model';
import {UserService} from '../../services/user.service';
import {ActivatedRoute} from '@angular/router';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  /** Search bar item. */
  @ViewChild('searchBar') searchBar;
  /** Subject instance. */
  unsubscribeAll: Subject<any> = new Subject();
  /** Configuration list */
  listConfiguration: ListingConfigInterface;
  /** Current page size */
  rpp = 6;
  /** Current page number.*/
  page = 1;
  /** Total size of current data */
  total = 0;
  /** Page size options */
  pageSizeOptions = [6];
  /** If component is loading data from the server. */
  isLoading: boolean;
  /** Users */
  users: User[];


  /**
   * Create a new instance from list component
   * @constructor
   * @public
   * @param {UserService} userService
   * @param {ActivatedRoute} route
   */
  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {
    // Get list config.
    this.listConfiguration = this.userService.getUserListConfig();
    // Get data from resolver
    this.users = this.userService.classifyResults(this.route.snapshot.data.data.data);
    this.rpp = this.users.length;
    this.total = this.route.snapshot.data.data.total;
  }

  /**
   * When component initialized.
   * @return {void}
   */
  ngOnInit(): void {
  }

  /**
   * Load data from the server side.
   * @return {void}
   */
  load(): void{
    // Make component in the loading mode
    this.isLoading = true;
    // Call http request from service.
    this.userService.getUserList(this.page).pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        results => {
            this.total = results.body.total;
            this.users = this.userService.classifyResults(results.body.data);
            this.rpp = this.users.length;
            this.isLoading = false;
        }, error => {
          console.log(error);
        }
      );
  }

  /**
   * When change pagination options
   * @param {any} event
   * @return {void}
   */
  onPaginateChange(event): void {
    this.rpp = event.pageSize;
    this.page = event.pageIndex + 1;
    this.load();
  }

  /**
   * Search on usar by id
   * @return {void}
   */
  searchUser(): void{
    // Check on search value.
    if (!this.searchBar.value || !parseInt(this.searchBar.value, 10)){
      this.load();
      return;
    }
    // Make component in loading mode
    this.isLoading = true;
    // Get results by id.
    this.userService.getSingleUserDetails(this.searchBar.value).pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        results => {
          if (results.data && results.data.id){
            this.users = [new User(results.data)];
            this.rpp = 1;
            this.total = 1;
            this.page = 1;
          }else {
            this.users = [];
          }
          this.isLoading = false;
        }, error => {
          this.users = [];
          this.isLoading = false;
          console.log('ERROR => ', error);
        }
      );
  }

  /**
   * When component destroyed.
   * @return {void}
   */
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

}
