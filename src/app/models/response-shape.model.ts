/** This interface will be shaped all response coming from server-side
 */
export interface ResponseShapeModel {
  page?: number;
  per_page?: number;
  total?: number;
  total_pages?: number;
  data?: any;
}
