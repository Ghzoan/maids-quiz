/** Single row configuration in the list. */
interface ListingConfigRowInterface {
  id?: string;
  value: any;
  label: string | number;
  cellHoverTooltip?: any;
  cellActions?: {icon: string, hideIfHoverEmpty?: boolean, hoverText?: string, click?: any}[]
}
/** Action configuration. */
export interface ListingConfigActionInterface {
  action: string;
  label: string;
  labelColor ?: string;
  backgroundColor ?: string;
  icon: string;
  color: string;
  handler?: any;
  visible: any;
  cellHoverTooltip?:any;
}
/** Details configuration. */
interface ListingConfigDetailsInterface {
  id?: string;
  value: any;
  label: string | number;
  visible: any;
}

/** List configuration should be passed to list component as input. */
export interface ListingConfigInterface {
  listing: ListingConfigRowInterface[];
  background?: (row: any) => string | string;
  actions?: ListingConfigActionInterface[];
  details?: ListingConfigDetailsInterface[];
}




