/** Shape of user */
export class User{
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;

  /**
   * Create a new instance from user
   * @constructor
   * @public
   * @param {UserInterface} userObj
   */
  constructor(userObj?: UserInterface){
    this.set(userObj);
  }

  /**
   * Create a new user instance from object.
   * @param {any} obj
   * @return {User}
   */
  set(obj: any): User {
    if (!obj) {
      return this ;
    }
    Object.entries(obj).map(([key, value]) => {
      this[key] = value ;
    });
    return this ;
  }
}

export interface UserInterface {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
